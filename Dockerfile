# This image builds the blog for us.
FROM alpine:latest AS hugo

RUN apk add --no-cache --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community hugo

WORKDIR /app
COPY src .

# Run Hugo
RUN hugo version
RUN hugo --minify

# Copy the static files to an Nginx webserver and serve them
FROM nginxinc/nginx-unprivileged:alpine
COPY --from=hugo /app/public /usr/share/nginx/html
