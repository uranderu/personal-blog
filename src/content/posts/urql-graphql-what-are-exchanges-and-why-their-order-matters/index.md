---
title: "Urql GraphQL what are exchanges and why their order matters"
date: "2023-02-15T18:12:30+02:00"
author: "Fabian"
description: "Urqls' exchanges are awesome! Learn more about them here and my stupid mistake."
showFullContent: false
hideComments: false
categories: ["Development", "GraphQL"]
tags: ["Development", "GraphQL"]
cover:
  image: "cover.webp"
  relative: true
---

# Introduction

So I recently ran into a nasty bug where [urql](https://formidable.com/open-source/urql/), the GraphQL client I was
using, seemed broken. Turned out I messed up the order of my exchanges. And I want to prevent others from making this
mistake. :)

## What are exchanges?

Exchanges are basically middleware. For those not really familiar with middleware in this context, middleware is a piece
of software which every request goes through. So you could for example have middleware which will add an arbitrary HTTP
header to every request.

## How do I add these exchanges to my urql config?

You can add these to your "createClient" function. Here is an example:

```typescript
import {createClient, dedupExchange, fetchExchange, cacheExchange} from 'urql';

const client = createClient({
    url: 'http://localhost:3000/graphql',
    exchanges: [dedupExchange, cacheExchange, fetchExchange],
});
```

Now by default, urql already uses the exchanges listed above. So you don't have to add these manually. **It is however
very important to note, once you define the exchanges, the defaults don't apply anymore.**

## Where I went wrong

So the issue I had was the following. I was using urql in a Next.js application which used
SSR ([more about that here](https://blog.fab1an.dev/posts/csr-ssr-and-ssg-explained-pros-and-cons-and-its-use-cases/))
and urql has a very nice [separate package](https://github.com/urql-graphql/urql/tree/main/packages/next-urql) for
Next.js which makes SSR very easy. Now I went ahead and put the "ssrExchange" at the end like this:

```typescript
import {dedupExchange, cacheExchange, fetchExchange} from '@urql/core';

import {withUrqlClient} from 'next-urql';

export default withUrqlClient(
    ssrExchange => ({
        url: 'http://localhost:3000/graphql',
        exchanges: [dedupExchange, cacheExchange, fetchExchange, ssrExchange],
    }),
    {ssr: true} // Enables server-side rendering using `getInitialProps`
)(Index);
```

Which is wrong because it should be like this:

```typescript
import {dedupExchange, cacheExchange, fetchExchange} from '@urql/core';

import {withUrqlClient} from 'next-urql';

export default withUrqlClient(
    ssrExchange => ({
        url: 'http://localhost:3000/graphql',
        exchanges: [dedupExchange, cacheExchange, ssrExchange, fetchExchange],
    }),
    {ssr: true} // Enables server-side rendering using `getInitialProps`
)(Index);
```

So the "fetchExchange" and the "ssrExchange" should be swapped. This took me a while to find out and this broke my
application to the point where it wouldn't fetch data anymore.

## Read more

Most of this information and examples I got from
the [urql documentation](https://formidable.com/open-source/urql/docs/). Don't make the same mistake as me and read it
thoroughly! ;)
