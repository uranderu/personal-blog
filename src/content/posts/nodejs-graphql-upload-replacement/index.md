---
title: "Nodejs graphql-upload Replacement"
date: "2022-11-21T17:19:30+02:00"
author: "Fabian"
description: "This drop-in replacement for graphql-upload is what you're searching for!"
showFullContent: false
hideComments: false
categories: ["Development", "NodeJS", "GraphQL"]
tags: ["Development", "NodeJS", "GraphQL"]
cover:
  image: "cover.webp"
  relative: true
---

# Introduction

Ever since [graphql-upload v14](https://github.com/jaydenseric/graphql-upload/releases/tag/v14.0.0), the maintainer has
made the controversial decision to force deep imports. This means that you can no longer do this:

```typescript
import {GraphQLUpload} from 'graphql-upload';
```

Now you have to change that to this:

```typescript
import GraphQLUpload from 'graphql-upload/GraphQLUpload.mjs';
```

Although this is dubious at best, this seems like a quick fix. It is, except when you use TypeScript.

## The Proposed "Solutions"

Now I'm not going to document all the proposed solutions from the maintainer and the community alike here. I've spent
quite a few hours trying to fix this myself. I've followed
[this](https://github.com/jaydenseric/graphql-upload/issues/305), [this](https://github.com/jaydenseric/graphql-upload/issues/282#issuecomment-1140303223), [this](https://stackoverflow.com/questions/72361047/error-no-exports-main-defined-in-graphql-upload-package-json), [that](https://github.com/jaydenseric/graphql-upload/issues/337#issuecomment-1240038750)
and probably a lot more when I went deep into the rabbit hole. But nothing seemed to work, and even if it did, the
"solutions" often meant changing your tsconfig.json to allow JavaScript and **** like that.

## The Actual Solution

Now whilst trying to solve this problem I stumbled on a graphql-upload alternative,
namely [graphql-upload-minimal](https://www.npmjs.com/package/graphql-upload-minimal).
It's a fork from graphql-upload which is literally a drop-in replacement. Even the TypeScript types are exactly like
graphql-upload pre v14.

This meant I just had to remove graphql-upload and its types package and install this one. Then change lines like this:

```typescript
import {FileUpload, GraphQLUpload} from 'graphql-upload';
```

To:

```typescript
import {FileUpload, GraphQLUpload} from 'graphql-upload-minimal';
```

And boom, it works exactly like before.

