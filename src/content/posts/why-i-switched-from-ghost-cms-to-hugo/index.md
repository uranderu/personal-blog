---
title: "Why I switched from Ghost CMS to Hugo"
date: "2022-06-30T13:54:00+02:00"
author: "Fabian"
description: "The reason why my blog was offline for two months!"
showFullContent: false
hideComments: false
categories: ["Development","Explainers"]
tags: ["Development","Explainers"]
cover:
  image: "cover.webp"
  relative: true
---

# Introduction

Recently an update to [Ghost CMS](https://ghost.org/) caused it to not work anymore with MariaDB. MariaDB isn't
officially supported on Ghost but since it's meant to be a drop-in replacement for MySQL in 9/10 cases it doesn't
present any issues. [Until it did](https://github.com/TryGhost/Ghost/issues/14634).

## What happened?

Apparently it was an upstream [issue](https://github.com/TryGhost/Ghost/issues/14634) with Knex. For those of you
wondering what Knex is:
> A batteries-included, multi-dialect (PostgreSQL, MySQL, CockroachDB, MSSQL, SQLite3, Oracle (including Oracle Wallet
> Authentication)) query builder for Node.js.

Knex generated invalid SQL for MariaDB which meant the migration wasn't working and the whole app couldn't start.

The issue was fixed and all should be good and well, except it wasn't. After that fix came the Ghost team had the
[following to say](https://github.com/TryGhost/Ghost/issues/14446) about the situation:
> Note: MariaDB is not an officially supported database for Ghost. It just happened to work given the similarities with
> MySQL, but we optimize and test for MySQL 5 and 8. As of Ghost 5.0 we are clarifying that official support is purely
> for
> MySQL8 in production so that we can double down on DB optimizations. We strongly recommend changing to MySQL8 and a
> helpful guide can be found here.

This caused a bit of situation because a lot of people rather dislike MySQL, and for good reasons. I use PostgreSQL
wherever I can, but if I need a MySQL compliant database I'll always use MariaDB. I'll write an article about it in the
future explaining why.

## Why is this an issue?

So I'm using [TrueNAS Scale](https://www.truenas.com/truenas-scale/) for about half a year now (I'll write about it in
the future) and run all my deployments via their "Apps". Which is K3S with a TrueNAS sauce over it. I mostly use the
community provided [TrueCharts apps](https://truecharts.org/) which are
Kubernetes [Helm Charts](https://helm.sh/docs/topics/charts/) with the TrueNAS sauce over it. Now when this was
happening me and a few others first thought it was a TrueCharts issue, but as established it wasn't. The TrueCharts team
also followed the situation and upon the above statement from the Ghost team dropped support for Ghost on TrueCharts. So
my instance was just sitting there waiting for a fix that would never come. I only realized this about a week ago when I
finished my semester at school and had time to look in to it again.

## Static Site Generation (SSG) vs Server Side Rendering (SSR)

So I needed an alternative. I've always been a fan of static websites, and I thought about it initially when starting my
blog with Ghost but due to a lack of experience in general webdev (and a lot in hosting) I went for Ghost. Now though
the situation was different.

I'm not going to explain the differences since I already did that in
another [post](/posts/csr-ssr-and-ssg-explained-pros-and-cons-and-its-use-cases/)

### Why I chose SSG

There are three reasons why I prefer SSG over SSR for my blog:

- SSR is quite honestly completely useless here. The only reason I can imagine for using it over SSG is when you have
  comments, but I don't have those.
- As stated I can't see an advantage for SSR here, while the advantages of SSG are clear: Security (no login)
  and **PERFORMAAAANCEEE!!**
- As explained in the other post, the shear ease of hosting. I only need to build the site and then simply host it with
  a webserver. No complex kubernetes services with databases.

So I think in my case it's quite clear, for a blog like this, SSG is the winner.

## Why Hugo?

I recently rebuild my entire [personal website](https://www.fab1an.dev) using
SvelteKit with the static site generator adapter. I was planning on doing the same for this blog, but then I thought:
"Wouldn't it be fun and more diverse to use a "real" static site generator?" So I then searched from some options, I
then
found:

- Next.js
- Gatsby
- Nuxt.js
- Hugo
- Jekyll

The first two immediately fell off because they use React. I have no experience with it but after reading 5 lines of
code I
immediately want to run back to Svelte.

I do have experience with Nuxt but if I wanted something like that I would use SvelteKit, since that's basically the
same but then with Vue instead of Svelte.

Hugo and Jekyll then seemed like the only two options but Hugo is more popular, looks more modern after my teeny tiny
research and uses Go which means they have an awesome mascot. I decided what the hell lets go with Hugo and here we are
now. I hope to one day write a more in depth review when I have more experience with it.
