---
title: "RouterOS why you should manage static IPs via DHCP"
date: "2022-07-20T17:22:40+02:00"
author: "Fabian"
description: "You've been taught wrong. Come over and let's do it the easy way. Not exclusive to RouterOS."
showFullContent: false
hideComments: false
categories: ["Tutorial","Networking","MikroTik","RouterOS"]
tags: ["Tutorial","Networking","MikroTik","RouterOS"]
cover:
  image: "cover.webp"
  relative: true
---

# Introduction

Now in my first year of Dutch college I specialized in what was called "Cybersecurity & Cloud" which is a specialization
heavily focused on infrastructure. And everywhere we were taught and instructed to assign static IPs on the client
itself. Now if you think about it for two seconds that's really stupid in real world applications. Aside from school I
also see this constantly in tutorials. Using DHCP has some great advantages, and you'd still want those, but you just
want it to assign the same IP to a specific MAC address over and over. Fortunately this is really easy in RouterOS and
with this quick tutorial I'll show you how.

## Advantages of (managing static addresses with) DHCP

Now personally I see three reason why you'd want this:

- Portability: And this is the main one. You don't need a lot of static addresses or big corperate networks to feel this
  one. What if your whole network moves to another DNS server? What if you're changing your subnet? The amount of time
  necessary to change every network config on every client with a static IP scales linearly. If you're running a lot of
  VMs or containers that are traditionally connected to your network I wish you the best of lucky. Because if you have a
  lot of them you're in a lot of pain.
- Overview: The bigger and more professional your network the more important a good overview of it is. There is a reason
  logging, monitoring and good dashboards are such nice to haves. Now imagine having a big network (which often have a
  lot of static IPs) where a lot of your addresses are not in here. I mean there are still ways to circumvent this
  disadvantage, but it's a lot easier if it's already there, which is the case in a lot of router interfaces.
- Managing address pools: Now depending on your router this can be a nuisance. If you need more room for your static IPs
  and you have to shrink your address pool it could be that after changing that the router will do nothing and wait
  untill the lease expires to give a new one from the recently updated pool. However, it's also possible when shrinking
  the pool it will update clients still using addresses from the old pool with new ones.

## Tutorial

Now as I'm a user of RouterOS I will show you how to manage static IPs via DHCP on RouterOS. If you're using something
else, I hope you can do something similar.

### Creating a static lease via DHCP

Go to your leases (IP -> DHCP Server -> Leases in Winbox) and click on a client. For this tutorial I've selected a 
Chromecast in my network. Now simply click the "Make Static" button.

Now you can check that you're client has now a static IP with the following methods.

- Still in "DHCP Lease" window make sure that the text in the table in the bottom left corner says "enabled" instead of
  "dynamic"
- In the "Leases" tab/overview make sure there is no "D" in the first column of your client row. The "D" stands for
  "Dynamic" for your information.

{{< figure src="images/create-static-lease.png" alt="">}}

### Changing the IP address of a static lease managed with DHCP

Now if you ever want to change the IP address you can do that via the "General" tab of the lease window. Do note that if
you want to immediately change this after creating a static IP for a client you need to exit out of the lease window and
go back in again. This is bad UX and MikroTik should change it, but this is the state of the game.

You can change the IP address by simply editing the values and pressing "Apply".

{{< figure src="images/editing-lease-ip.png" alt="">}}

## Conclusion

RouterOS makes it dead simple to do this. I think the advantages are enormous, and I know it probably doesn't matter a
lot in PoC kind of projects but still, it's easier to do it this way than to finding the client OS specific way of
changing it. And what if it grows to not be a PoC anymore ;) ?
