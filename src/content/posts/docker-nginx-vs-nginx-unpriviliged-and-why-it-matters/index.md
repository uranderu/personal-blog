---
title: "Docker Nginx vs Nginx Unprivileged and why it matters (a lot)"
date: "2023-02-16T18:12:30+02:00"
author: "Fabian"
description: "Using Nginx in Docker is great, but their official images are actually dangerous!"
showFullContent: false
hideComments: false
categories: ["Architecture", "Docker"]
tags: ["Architecture", "Docker"]
cover:
  image: "cover.webp"
  relative: true
---

# Introduction

This is something that baffled me when I found out. And since
the [official Nginx docker image](https://hub.docker.com/_/nginx) is downloaded over a billion times, I think it is very
important to get this out.

## What is going on?

So the official Nginx Docker image runs in what is called "privileged mode". When you run a container in privileged mode
you essentially run it as root. This means that the container has access to the complete underlying system. This in
contrast to non-privileged mode where the container runs as a random user and group which doesn't have access to
anything but their own container.

## Why is this bad?

Because when you run application or containers as root you are giving them a free pass to do whatever they want. And
although Nginx can probably be trusted, what if they get hacked and a malicious image is published? Or what if there is
a vulnerability in Nginx itself and someone gets access to your container? That would mean they wouldn't need
any [privilege escalation](https://en.wikipedia.org/wiki/Privilege_escalation). Because they are already the boss, and
remember, not of the container, of the entire host.

These are just a few examples of how this could be abused. And the thing is, it is totally unnecessary!

## How do we fix this?

So, the answer is really simple and actually funny enough comes from Nginx themselves. They also mention this in the
documentation of the base image. But there is a separate image
called ["nginx-unprivileged"](https://hub.docker.com/r/nginxinc/nginx-unprivileged) which fixes this issue completely!
You use the image in the same way you use the "official" image. But **do note, this version uses port 8080 instead of 80**
so set up your port forwarding correctly!
