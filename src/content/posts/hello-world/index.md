---
title: "Hello world"
date: "2022-06-30T13:54:00+02:00"
author: "Fabian"
description: "Hi everyone, and welcome back to my blog!"
showFullContent: false
hideComments: false
toc: false
categories: ["Notice"]
tags: ["Notice"]
cover:
  image: "cover.webp"
  relative: true
---

# Hi everyone, and welcome back to my blog!

For those who don't know, it's been offline for like two months :O ([Explanatory blogpost](/posts/why-i-switched-from-ghost-cms-to-hugo/)).

But for those of you who are new here, welcome! My name is Fabian and on this blog I write about my adventures as an IT
fanatic/student/employee. You can expect opinions (especially a lot of those), tutorials and explainers.

Unfortunately due to some hosting issues I'll have to rewrite my old posts, but I'm sure I'll be up to speed shortly.
You can also expect some minor tweaks to the blog since I'm using [HUGO](https://gohugo.io/) now, and I'm completely new
to it. :3
