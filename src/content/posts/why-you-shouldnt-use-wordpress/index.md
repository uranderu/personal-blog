---
title: "Why you shouldn't use WordPress"
date: "2022-08-14T17:53:00+02:00"
author: "Fabian"
description: "WordPress is grossly misused. It's blogging software, stop using it for everything except that."
showFullContent: false
hideComments: false
categories: ["Explainers"]
tags: ["Explainers"]
cover:
  image: "cover.webp"
  relative: true
---

# Introduction

A lot of people think I hate WordPress, I really don't. But I hate the way its grossly misused across the web.

In this post I'll briefly talk about what WordPress is, since everybody keeps forgetting it. Why you shouldn't use it
and why this matters.

## Why this matters

Simple, it is used **A LOT**. According to [Kinsta](https://kinsta.com/wordpress-market-share/) it has a market share of
43%. And to be clear, that is of all websites. That is huge!

## What is WordPress?

WordPress is blogging software. And yes, you can install 60 plugins and create a web-shop with it. Or 20 and have a
business site. But it will and has always been blogging software.

Later on I'll elaborate why this is important.

## Reasons to not use WordPress

To the main event then, shall we?

### Its being used for things it wasn't made to do

As previously stated WordPress is blogging software. Now you can do other things with it, like a web-shop or a simple
business site. But honestly it's like screwing in a screw with a hammer. Yes it kind of works, but I'd rather have the
screwdriver. You?

I mean you can already see evidence of this on the simplest of websites. Just look at all the plugins every WordPress
site needs. You need a theme plugin otherwise it looks like shit, you need Yoast to have half decent SEO. Don't forget
Jetpack for all the most basic functionality like backups. And then to combat the security nightmare that is WordPress
you need something like Wordfence.

### Performance

WordPress needs and uses a database to store most of its content. Now every time someone visits your site, it needs to
retrieve data from the database, then the actual page needs to be generated on the server, and then it gets send to you.

Now there are some tricks and a bunch of plugins that you can install to improve this process. But in any case you're
stuck with this architecture.

Now there is nothing inherently wrong with this architecture, but it's totally unnecessary for most sites. Compare it
for example to this blog where all pages are statically generated at build time. This means that no matter where you go
on my blog, the pages are already there. Nothing to be retrieved from a database, and nothing to be generated on the fly
on a server. The drastic simplification of everything has **HUGE** performance advantages. Which provides a better
experience for owner and visitor.

### Cost

As previously established, WordPress requires a bit of architecture. You, the owner needs to pay for this architecture.
Now most WordPress hosting packages are quite cheap. But you're still paying for it. You know how dirt, dirt cheap it is
to host a static site? It's just a bunch of files that need to be served by a webserver. But to be honest, this will
only be an issue once you need to scale. I have no statistics to back this up but most websites are not visited that
often to really think about scale. But once you really need to do, it'll be a pain in you butt.

### Security

Now I know, I know. Nobody cares about security until it is too late. But still, think about it. The huge usage of
WordPress also means there is a giant and I mean **GIANT** target on it's back. Finding a good vulnerability in
WordPress is worth a lot. As someone who hosts a couple of very small websites, I can see that I'm constantly getting
scanned by bots to see if I'm using WordPress. And it's not only scans, its also common vulnerabilities that they
immediately try to exploit. Even though they don't know if I even run WordPress. But that doesn't matter, because 43%
of all websites do.

### The companies that "develop" these sites

I want to make one thing very clear here. Your lack of development and general IT knowledge is getting exploited at
these companies. You're being scammed. The run-of-the-mill companies that "develop" these websites all do the same
thing:

1. Buy a cheap and managed hosting plan
2. Buy or install a free theme
3. Replace the sample text with the customers information
4. Profit

They are not an IT company, and they don't have developers. They just have monkeys who do this work all day and call
themselves developers. Of course, they still charge you the standard developer rate. And for your information, those
steps can be done in an hour.

Afterwards they'll probably offer you a support contract to keep everything up to date. Something which is completely
unnecessary if you didn't use WordPress.

## The solution

I've mentioned it here and there. But most sites should go for a static(ally generated) website. It doesn't really
matter how it gets there, just that it is. And I know a lot of people and companies want a WordPress site because they
can change the content itself. But that's not really a reason to choose WordPress. First off no company doesn't nearly
change their site as much as they initially think they'll do. And second, static websites aren't set in stone. Just ask
the developer or the company to change out this text here or that image there, and they'll do it in the source code.
That's the easiest task you can give an *actual* developer. 
