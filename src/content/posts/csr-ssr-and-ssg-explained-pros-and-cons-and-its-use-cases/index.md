---
title: "CSR, SSR and SSG. Explained, pros and cons and its use cases"
date: "2022-07-13T17:05:00+02:00"
author: "Fabian"
description: "As usual, it depends!"
showFullContent: false
hideComments: false
categories: ["Development","Explainers"]
tags: ["Development","Explainers"]
cover:
  image: "cover.webp"
  relative: true
---

# Introduction

In this post I'll explain the different ways you can render a webpage, their pros and cons and some examples where I
think some methods are better suited than others.

## CSR (Client Side Rendering)

In CSR only the bare minimum for the initial page is sent by the server. Any other logic, data fetching, templating and
routing required to display content on the page is handled by JavaScript code that executes in the browser/client.

### Pros and cons

- Although the [FCP](https://web.dev/fcp/) (First Contentful Paint) of CSR apps are relatively slow. All subsequent
  "actions" are fast because it doesn't need a server to do anything.
- It'll hurt your SEO. Although web crawlers are getting better and better at crawling CSR webpages, it's still quite
  bad to other rendering techniques where it directly gets fed HTML.
- If you have a CRUD (Create, Read, Update and Delete) application you probably need some kind of (REST) API to interact
  with your database.
- Building PWA (Progressive Web Apps) is easier than building native apps for Android and iOS. PWAs are webapps that
  look, feel and execute the same tasks as traditional apps.
- Cost. Your clients are doing most of the heavy lifting.

### Use cases

To be quite honest I'm not the biggest fan of CSR. Aside from the cost advantage I'm not really sure what the **BIG**
advantage of CSR is. If you have a resource intensive webapp I can see why you would like to reduce cost but then again
that's still the same argument.

## SSR (Server Side Rendering)

When SSR is used the initial page is rendered on a server and send to the client.

### Pros and cons

- Compared to CSR it's a good fit for SEO dependant webapps.
- You often don't need a separate API for interacting with your database. By definition, it isn't more secure, but it is
  one more thing that isn't exposed to the internet and quite honestly one more thing you don't need to worry about.
- If you have a traffic heavy website it can be quite expensive. Of course, it depends on what you are SSR but by
  definition it means having a server that's rendering webpages for your users.

### Use cases

This method is suited for webpages where content changes often but SEO and a more minimal complexity is important. I
think the SEO point is quite clear but not having to host an API for you front-end but instead interacting directly with
your database is in my opinion totally worth it.

## SSG (Static Site Generation)

Now this is a bit of an odd one out because it's more of a tool than a way to render pages. Sort of. You see SSG refers
to tools like [Hugo](https://gohugo.io/) which I use for this blog. Essentially what tools like Hugo do is static site
generation, which is the process of transforming data/templates into a full static HTML website. You use a static
site generator to generate your site, the advantage of the generator is that it automates the coding of the HTML/CSS
webpages.

You don't need tools like Hugo to build a static website, but it helps. So when I further refer to SSG I'm actually
referring to static websites.

### Pros and cons

- The big and I mean **BIG** advantage of this is that your webpages are generated at build time which means that when a
  user requests a webpages the webserver simply needs to transfer the files to the user. No rendering on the client nor
  on a server. There is no faster rendering method than this.
- Security. A lot of websites have some kind of CMS (Content Management System) to manage their content (Wordpress (SSR)
  for example). The problem with this is that's another login to protect. With SSG there is no login, there is only the
  files which are served by a webserver. Of-course you could hack the webserver, but that's also the case with other
  rendering methods. What I'm talking about is the layer which simply doesn't exist when using static files.
- Hosting. This is something in which I personally have a lot of experience. One of the cheapest and by far the easiest
  deployment is a static website. There's not a lot that can go wrong with that. I mean imagine hosting a static
  website (which is just a bunch of files) vs hosting a database and a server for SSR or hosting a database with an API
  for CSR to interact with. It's just cheaper and easier.
- Because every bit gets rendered at build time, it's practically impossible to use SSG for a very dynamic website.
  Imagine Meta having to rebuild Instagram every time someone posts, likes or comments on a post.

### Use cases

Static sites are awesome and honestly my favorite. It's great for websites where content doesn't change often. Say a
blog (like this one) or your typical business websites. The decreased complexity, security and ease of hosting make it
perfect for most websites.

## Conclusion

So what's right for you? The answer might well be a bit of all. And that's exactly where I wanted to end. For a simple
business website SSG is the king in my opinion. But if you have a complex webapp you might want the best of all worlds.
That's why for example popular frameworks like [Next.js](https://nextjs.org/) are giving you the option which routes you
want to CSR, SSR or SSG.

But in my opinion my decision goes like this: Choose SSG, is my project to dynamic for SSG? Is SEO and FCP important for
my project? Choose SSR, is it not important or is there another compelling reason to use CSR? Use that.
