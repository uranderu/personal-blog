---
title: "Migrating Websites from SvelteKit to Astro, the results"
date: "2023-04-11T18:12:30+02:00"
author: "Fabian"
description: "I recently migrated a couple of static websites from SvelteKit to Astro. In this post I'll discuss the results."
showFullContent: false
hideComments: false
categories: ["Development", "Explainers"]
tags: ["Development", "Explainers"]
cover:
  image: "cover.webp"
  relative: true
---

# Introduction

I have had my eye on [Astro](https://astro.build/) for a while. It looked like the perfect fit for the static websites
that I manage. But because of the following reasons, I didn't want to switch:

- I was/am very satisfied with SvelteKit's static adapter.
- There was no support for Astro in Webstorm (my IDE).
- I didn't know that the [prefetch](https://docs.astro.build/en/guides/integrations-guide/prefetch/) addon existed. I
  use something similar in SvelteKit and didn't want to migrate some sites without that functionality.

## What changed?

Well, since Webstorm 2023.1 there is an Astro plugin. Combine that with some curiosity and my discovery of the prefetch
addon and off I went.

## How did migrating go?

Very smooth and very easy. I can't condense it more than that. I didn't use a lot of Svelte specific functionality, so I
could pour over almost everything one to one. What is great is that you can just write TypeScript within an Astro
component like many other frameworks. So even the tiny bit of TypeScript that I used was more or less copy pasta.

### Component Migration

Specifying props is really similar. To give an example, here is one of my old components in SvelteKit:

```typescript
<script lang="ts">
  export let imagePath: string;
  export let name: string;
</script>

<li class="flex items-center p-2">
  <img alt="" class="mr-1 w-12 p-1" src="{imagePath}" width="48" height="48" />
  <p class="text-2xl dark:text-white">{name}</p>
</li>
```

And here it is in Astro:

```typescript
---
interface Props {
  imagePath: string;
  name: string;
}

const { imagePath, name } = Astro.props;
---

<li class="flex items-center p-2">
  <img alt="" class="mr-1 w-12 p-1" src={imagePath} width="48" height="48" />
  <p class="text-2xl dark:text-white">{name}</p>
</li>
```

### Other framework functionality

Layouts and slots also work more or less the same. The only thing I struggled with for a bit was that I used the Svelte
onMount function in one of my components. But by simply moving that functionality into a script tag below the HTML I
achieved the same functionality.

### Tooling

SvelteKit and Astro share a lot of the same tooling and integrations. So that meant that I could still
use [Vite](https://vitejs.dev/), [Playwright](https://playwright.dev/), [TailwindCSS](https://tailwindcss.com/), [Prettier](https://prettier.io/), [ESLint](https://eslint.org/)
etc. Some configuration changes were needed, but that was also very straightforward thanks to examples and
documentation.

### Want to see more?

If you want to see a side by side comparison, I can recommend looking into
the [PR](https://gitlab.com/uranderu/personal-website/-/merge_requests/3/diffs#867347a6a826929c2eab854375c2a729d28daef3)
for my personal website.

## The Results (Performance!!)

Because Astro has a "zero-JS frontend architecture" I expected a smaller, and thus faster, website. Because even when
you compile to a static website using SvelteKit, there still is some JS that gets shipped. So, by switching to Astro,
how much smaller did my sites get?

### Personal Website

My personal website, built using SvelteKit:

{{< figure src="images/personal-website-before.webp" alt="412 Kb total, load 525ms">}}

My personal website, built using Astro:

{{< figure src="images/personal-website-after.webp" alt="351 Kb total, load 280ms">}}

So in terms of total storage my website is now 14,77% smaller. But I think more important is the load, which is now
almost twice as fast (46,67%).

### Client Website

I also converted the website of one of my clients, which has a lot more going on.

Client website, built using Sveltekit:

{{< figure src="images/client-website-before.webp" alt="1,27 MB total, load 915ms">}}

Client website, built using Astro:

{{< figure src="images/client-website-after.webp" alt="731 kB total, load 446ms">}}

As you can see, the improvements are relative, not absolute. Which is great! In terms of storage this website is now
42,44% smaller! And the load is now more than twice as fast (51,26%). The requests are now also almost halved.

## Conclusion

These numbers are fantastic, but I have to admit that they probably won't matter in most of the world. I mean both sites
already had a 100 score on the [Google Lighthouse](https://developer.chrome.com/docs/lighthouse/overview/) performance
test. But as far as I am aware, there now is no dead code shipped. Which means I did the best that I could. And that
matters to me. :)
