---
title: "RouterOS how to block users from setting a static IP on a network with DHCP"
date: "2022-07-20T17:20:30+02:00"
author: "Fabian"
description: "Your network, your rules! This tutorial explains how to block user set static IPs on a network that uses DHCP in RouterOS."
showFullContent: false
hideComments: false
categories: ["Tutorial","Networking","MikroTik","RouterOS"]
tags: ["Tutorial","Networking","MikroTik","RouterOS"]
cover:
   image: "cover.webp"
   relative: true
---

# Introduction

Most network use DHCP to assign IP addresses to clients on the network. Now imagine someone (un)intentionally assigning
an IP address that is already in use. This can have irritating to disastrous results. To illustrate: imagine taking over
an IP of a printer, which is irritating. And now imagine taking over the address of a server or a router. Quite bad,
isn't it? **Note: This is only for IPv4, ARP can be used for IPv6 but is uncommon. The IPv6 equivalant of ARP is
[NDP](https://en.wikipedia.org/wiki/Neighbor_Discovery_Protocol).**

## ARP spoofing

Another quite serious security implication if this behavior is allowed is that the router is vulnerable to ARP spoofing.
For those who don't know what ARP and/or ARP spoofing is Wikipedia
defines [ARP](https://en.wikipedia.org/wiki/Address_Resolution_Protocol) as:

> The Address Resolution Protocol (ARP) is a communication protocol used for discovering the link layer address, such as
> a MAC address, associated with a given internet layer address, typically an IPv4 address. This mapping is a critical
> function in the Internet protocol suite. ARP was defined in 1982 by RFC 826, which is Internet Standard STD 37.

And [ARP Spoofing](https://en.wikipedia.org/wiki/ARP_spoofing) as:
> In computer networking, ARP spoofing, ARP cache poisoning, or ARP poison routing, is a technique by which an attacker
> sends (spoofed) Address Resolution Protocol (ARP) messages onto a local area network. Generally, the aim is to
> associate
> the attacker's MAC address with the IP address of another host, such as the default gateway, causing any traffic meant
> for that IP address to be sent to the attacker instead.

### Example

So to put it in an example, at the moment the router can and will ask for example: "Hi, who knows the MAC address of
10.0.0.2?". And anyone can answer. When the router gets a response it will save this in its ARP cache. Now normally
clients will respond only with correct answers as they aren't inherently maleficent. But in the real world there are
maleficent clients (i.e. hackers) and now imagine that it turns out that the client that responded with: "I'm 10.0.0.2
and my MAC address is AB:AB:AB:AB:AB:AB" isn't actually who he says he is. The real 10.0.0.2 is a very important server
which receives sensitive information. Now because the cache is spoofed, the router will send the data to the wrong
client.

## How we'll protect ourselves

In RouterOS we can protect us from this vulnerability quite easily, as long as we use DHCP for every client on our
network. If you don't do this, you should. If you want static addresses for a few clients there is a
[better way](/posts/routeros-why-you-should-mange-static-ips-via-dhcp) to do it than on the client itself.

We'll protect us as follows:

1. First we stop the router from accepting or requesting MAC addresses via ARP.
2. Now how do we know which IP address matches which MAC address? Simple, our DHCP server. It knows the MAC addresses of
   each of its clients. So every time a new IP is given we'll update the ARP accordingly.

## Tutorial

Now that we know how to protect ourselves we'll implement it. Just to be safe, we're going to implement step two first
before the unavailability of ARP is going to cause trouble.

### Add ARP entries via a DHCP server

Go to your DHCP server. (IP -> DHCP Server in Winbox) For this tutorial I'll be using the DHCP server from my guest
network. And check the "Add ARP For Leases" box.

{{< figure src="images/dhcp-server-add-arp.png" alt="">}}

### Stop the router from accepting or requesting MAC addresses via ARP.

To do this go to the interface (Interfaces in Winbox) you run your DHCP server on. This can be for example be a bridge
or a VLAN. For this tutorial I'll be using my guest VLAN interface. In the interface select "reply-only" from the
"ARP" dropdown menu.

{{< figure src="images/interface-reply-only.png" alt="">}}

## Conclusion

That's it. Yep, no jokes. Your router is now protected from ARP spoofing attacks and when you accidentally forget to
remove your servers IP address from your PC's ethernet interface after testing (whoops) you don't get weird connection
issues.
